package ua.com.avesoft.base.utils

import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch

object Asynchronous {

    fun countDown(count: Int, period: Long = 1000, block: T1Handler<Int>): Handler {
        val job = launch {
            repeat(count) {
                delay(period)
                launch(UI) { block(count - it - 1) }
            }
        }
        return { job.cancel() }
    }
}