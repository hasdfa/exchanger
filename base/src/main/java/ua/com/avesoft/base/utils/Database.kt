package ua.com.avesoft.base.utils

import android.database.Cursor
import ua.com.avesoft.base.database.*
import java.security.InvalidAlgorithmParameterException
import java.security.NoSuchAlgorithmException

object Database {

    /**
     * get cursor, and read info from it
     */
    fun<T: NNEntity> parseCursor(clazz: Class<T>, cursor: Cursor): Pair<List<T>, Exception?> {
        val list = arrayListOf<T>()
        try {
            if (cursor.moveToFirst()) {
                do {
                    val emptyObject = clazz.newInstance()
                    val fields = emptyObject.fields.map {
                        it.copy(value = cursor.getValue(it))
                    }
                    list += emptyObject.apply { fillData(fields) }
                } while (cursor.moveToNext())
            }
            cursor.close()
        } catch (e: Exception) {
            e.printStackTrace()
            return Pair(emptyList(), e)
        }
        return Pair(list, null)
    }

    /**
     * generating WHERE SQL claus
     *
     * if params length = 0, we don`t need it
     * else join parameters and add where
     */
    fun generateWhereClause(params: List<NNQueryParams>)
            = if (params.count() == 0) ""
    else "WHERE (${joinParams(params)})"

    /**
     * join params into SQL query
     *
     * Sample:
     * (name = 'Vadik') AND (surname LIKE 'Raksha')
     */
    fun joinParams(params: List<NNQueryParams>): String {
        if (params.count() > 1 && params.count() % 2 == 0)
            throw NoSuchAlgorithmException("length of params must be (>= 1 and odd) or 1 have: ${params.count()}")

        var i = 0
        return params.joinToString {
            if (i % 2 == 1 && it.name != NNQueryParams.connectorName) {
                throw InvalidAlgorithmParameterException("All odd params should be connectors, but $i isn`t: $it")
            }
            i++
            return@joinToString if (it.name == NNQueryParams.connectorName)
                it.value.toString()
            else
                "(${it.name} ${it.equalType.translateSQL()} ${it.value.translateSQL()})"
        }
    }

    /**
     * useful extension
     */
    fun Cursor.getValue(field: NNField): Any {
        val column = getColumnIndex(field.name)
        return when (field.type) {
            FieldType.TEXT -> getString(column)
            FieldType.INTEGER -> getInt(column)
            FieldType.FLOAT -> getFloat(column)
            FieldType.LONG -> getLong(column)
        }
    }

    /**
     * custom equalTypes transformation (for SQL)
     */
    fun EqualsType.translateSQL(): String = when (this) {
        EqualsType.EQUAL -> "="
        EqualsType.LIKE -> "LIKE"
        EqualsType.GREATER -> ">"
        EqualsType.LOWER -> "<"
        EqualsType.GREATER_OR_EQUAL -> ">="
        EqualsType.LOWER_OR_EQUAL -> "=<"
    }

    /**
     * custom field value transformation (for SQL)
     */
    fun Any.translateSQL(): String {
        if (this is CharSequence) {
            return "'$this'"
        }
        return this.toString()
    }

}