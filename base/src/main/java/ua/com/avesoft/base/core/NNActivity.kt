package ua.com.avesoft.base.core

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity

@SuppressLint("Registered")
abstract class NNActivity: AppCompatActivity(), NNFunctions {

    override fun context() = this

}