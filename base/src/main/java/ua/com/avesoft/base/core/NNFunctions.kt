package ua.com.avesoft.base.core

import android.content.Context
import android.content.res.Configuration
import android.graphics.drawable.Drawable
import android.support.annotation.ColorInt
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import ua.com.avesoft.base.R
import ua.com.avesoft.base.utils.Handler


/**
 * Created by Raksha Vadim on 09.07.18, 23:18.
 */
interface NNFunctions {

    //
    // MARK: For ui based functions
    //

    fun context(): Context
    fun activity() = context() as NNActivity



    //
    // MARK: UI helpers
    //

    /**
     * Show error message as AlertDialog
     */
    fun showError(message: String, onDismiss: Handler? = null) {
        if (message.length < 25)
            showAlert(title = message, dismiss = onDismiss)
        else
            showAlert(title = "Error", body = message, dismiss = onDismiss)
    }

    /**
     * Simple show message as AlertDialog
     */
    fun showMessage(title: String, body: String? = null, onDismiss: Handler? = null) {
        showAlert(title, body, dismiss = onDismiss)
    }

    /**
     * Show alert base implementation
     */
    fun showAlert(title: String, body: String? = null, ok: Handler? = null, cancel: Handler? = null, dismiss: Handler? = null) {
        if (activity().isDestroyed) return

        val alert = AlertDialog.Builder(context())
        val dialog: AlertDialog?
        alert.setTitle(title)
        body?.let { alert.setMessage(it) }
        alert.setPositiveButton(string(R.string.ok_button)) { d, _ ->
            ok?.invoke()
            d.dismiss()
        }

        // if cancel handler is not null, add `Cancel` button
        cancel?.let {
            alert.setNegativeButton(string(R.string.cancel_button)) { d, _ ->
                cancel.invoke()
                d.dismiss()
            }
        }
        alert.setOnDismissListener {
            dismiss?.invoke()
        }
        dialog = alert.create()
        dialog.show()
    }





    //
    // MARK: deviceInfo
    //

    val configuration: Configuration
        get() = context().resources.configuration

    val isLandscape: Boolean
        get() = with(configuration) { screenWidthDp > screenHeightDp }

    val isTablet: Boolean get() = configuration.smallestScreenWidthDp >= 620
    val isSmallTablet: Boolean get() = configuration.smallestScreenWidthDp in 620..700
    val isMidTablet: Boolean get() = configuration.smallestScreenWidthDp in 700..799
    val isBigTablet: Boolean get() = configuration.smallestScreenWidthDp >= 800







    //
    // MARK: resources
    //

    @ColorInt
    fun color(@ColorRes colorId: Int): Int = ContextCompat.getColor(context(), colorId)
    fun string(@StringRes stringId: Int): String = context().resources.getString(stringId)
    fun drawable(@DrawableRes drawableId: Int): Drawable? = ContextCompat.getDrawable(context(), drawableId)

    val colorPrimary        : Int get() = color(R.color.colorPrimary)
    val colorPrimaryDark    : Int get() = color(R.color.colorPrimaryDark)
    val colorAccent         : Int get() = color(R.color.colorAccent)
}