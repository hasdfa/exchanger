package ua.com.avesoft.base.utils.contstants

import android.annotation.SuppressLint

object Strings {

    const val DELETION_SERVICE_NAME = "ua.com.avesoft.nnproject.deletion_service"
    const val B_APP_ACTION = "ua.com.avesoft.nnproject.OPEN"
    const val FILE_PROVIDER_AUTHORITY = "ua.com.avesoft.nnproject.provider"
    const val RECEIVER_IMAGE_DELETED = "ua.com.avesoft.nnproject.IMAGE_DELETED"

    const val QUERY_MODE = "__internal_query_mode"
    const val UID_EXTRAS = "uid"
    const val IMAGE_URL_EXTRAS = "url"
    const val FILE_NAME_EXTRAS = "filename"
    const val STATUS_NAME_EXTRAS = "status"
    const val TIMESTAMP_NAME_EXTRAS = "timestamp"


    const val CHANNEL_ID = "nnb-notifications"
    const val CHANNEL_NAME = "NN Project(B)"


    // По тз сказали, а так бы использовал Environment.getExternalStorageDirectory()
    // Или кеш приложения: File.createTempFile()
    @SuppressLint("SdCardPath")
    const val BASE_FILE_PATH = "/sdcard/BIGDIG/test/B"


    const val MODE_ADD = "add"
    const val MODE_OPEN = "open"


    const val PARAM_FILTER = "__internal_filter"
    const val PARAM_SORT = "__internal_sort"
}