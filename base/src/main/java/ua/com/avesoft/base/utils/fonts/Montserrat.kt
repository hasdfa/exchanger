package ua.com.avesoft.base.utils.fonts

import android.content.Context
import android.graphics.Typeface

object Montserrat {

    fun get(context: Context, type: MontserratType): Typeface {
        return Typeface.createFromAsset(context.assets, "montserrat/${getFilename(type)}")
    }

    private fun getFilename(type: MontserratType) = when (type) {
        MontserratType.Black -> "montserrat_black.ttf"
        MontserratType.BlackItalic -> "montserrat_black_italic.ttf"
        MontserratType.Bold -> "montserrat_bold.ttf"
        MontserratType.BoldItalic -> "montserrat_bold_italic.ttf"
        MontserratType.ExtraBold -> "montserrat_extra_bold.ttf"
        MontserratType.ExtraBoldItalic -> "montserrat_extra_bold_italic.ttf"
        MontserratType.ExtraLight -> "montserrat_extra_light.ttf"
        MontserratType.ExtraLightItalic -> "montserrat_extra_light_italic.ttf"
        MontserratType.Italic -> "montserrat_italic.ttf"
        MontserratType.Light -> "montserrat_light.ttf"
        MontserratType.Regular -> "montserrat_regular.ttf"
        MontserratType.LightItalic -> "montserrat_light_italic.ttf"
        MontserratType.Medium -> "montserrat_medium.ttf"
        MontserratType.MediumItalic -> "montserrat_medium_italic.ttf"
        MontserratType.SemiBold -> "montserrat_semibold.ttf"
        MontserratType.SemiBoldItalic -> "montserrat_semibold_italic.ttf"
        MontserratType.Thin -> "montserrat_thin.ttf"
        MontserratType.ThinItalic -> "montserrat_thin_italic.ttf"
    }
}

enum class MontserratType {
    Black, BlackItalic,
    Bold, BoldItalic,
    ExtraBold,
    ExtraBoldItalic,
    ExtraLight,
    ExtraLightItalic,
    Regular, Italic,
    Light, LightItalic,
    Medium, MediumItalic,
    SemiBold, SemiBoldItalic,
    Thin, ThinItalic,
}