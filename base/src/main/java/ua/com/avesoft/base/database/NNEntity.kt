package ua.com.avesoft.base.database

import java.util.*

abstract class NNEntity {

    /**
     * Object unique universal identifier
     */
    var uid: String = UUID.randomUUID().toString()

    /**
     * Gets fields which will be save in database
     */
    abstract val fields: List<NNField>

    /**
     * Fills object with data from database
     */
    fun fillData(fields: List<NNField>) {
        fields.forEach {
            when {
                it.name == "uid" -> uid = it.value.toString()
                else -> internalFillData(it)
            }
        }
    }

    /**
     * Fills the child class object fields
     */
    abstract fun internalFillData(field: NNField)

    /**
     * Validates object before write to database
     */
    abstract fun validate(): Boolean

    /**
     * Default identifier field (unique universal identifier: UUID)
     */
    protected val uidField = NNField(
            name = "uid",
            value = uid,
            type = FieldType.TEXT
    )
}