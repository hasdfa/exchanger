package ua.com.avesoft.base.database

/**
 * Wrapper for database field
 */
data class NNField(
        val name: String,
        val value: Any? = null,

        val type: FieldType,
        val nullable: Boolean = false
) {

    val get: Any
        get() = value ?: defaultValue

    /**
     * Default field value by declared type
     */
    val defaultValue: Any
        get() = when(type) {
            FieldType.TEXT -> "''"
            FieldType.INTEGER -> -1
            FieldType.FLOAT -> -1.0
            FieldType.LONG -> -1
        }
}

/**
 * Supports field types
 */
enum class FieldType {
    TEXT,
    INTEGER,
    FLOAT,
    LONG,
}