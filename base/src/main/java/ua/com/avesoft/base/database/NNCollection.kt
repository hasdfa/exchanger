package ua.com.avesoft.base.database

import android.database.Cursor

interface NNCollection {

    /**
     * gets all records in collection
     */
    fun<T: NNEntity> all(clazz: Class<T>, sort: NNSortParam? = null,  vararg params: NNQueryParams): Pair<List<T>, Exception?>

    /**
     * gets cursor for all records
     */
    fun<T: NNEntity> cursorAll(clazz: Class<T>, sort: NNSortParam? = null,  vararg params: NNQueryParams): Cursor

    /**
     * gets one record
     */
    fun<T: NNEntity> one(clazz: Class<T>, sort: NNSortParam? = null,  vararg params: NNQueryParams): Pair<T?, Exception?>

    /**
     * inserts record in collection
     */
    fun<T: NNEntity> insert(t: T): Exception?

    /**
     * saves existed record in collection
     */
    fun<T: NNEntity> save(t: T): Exception?

    /**
     * removes document by id (uid)
     */
    fun removeId(uid: String): Pair<Int, Exception?>

    /**
     * removes document by id (uid)
     */
    fun remove(vararg params: NNQueryParams): Pair<Int, Exception?>

}