package ua.com.avesoft.base.database

/**
 * Query param wrapper for {@link ua.com.avesoft.base.database.NNCollection}
 */
data class NNQueryParams(
        val name: String,
        val value: Any,
        val equalType: EqualsType = EqualsType.EQUAL
) {
    companion object {
        /**
         * Feature to connect query params with logic operators
         */
        const val connectorName = "__internal_connector"

        /**
         * Logic operator 'OR'
         */
        public val OR = NNQueryParams(connectorName, "OR")

        /**
         * Logic operator 'AND'
         */
        public val AND = NNQueryParams(connectorName, "AND")
    }
}

/**
 * Equal types between query param name and value
 */
enum class EqualsType {
    EQUAL,
    LIKE,

    GREATER,
    LOWER,

    GREATER_OR_EQUAL,
    LOWER_OR_EQUAL,
}

/**
 * Sort wrapper for {@link ua.com.avesoft.base.database.NNCollection}
 */
data class NNSortParam(
        val name: String,
        val sortDirection: SortDirection = SortDirection.ASC
)

/**
 * Supported sort directions
 */
enum class SortDirection {
    DESC,
    ASC,
}