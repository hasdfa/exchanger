package ua.com.avesoft.base.database

interface NNDatabase {

    /**
     * Gets all collection from current database instance
     */
    fun collections(): List<String>

    /**
     * Gets collection by name
     */
    fun collection(name: String): NNCollection

    /**
     * Creates new collection
     */
    fun createCollection(name: String, fields: List<NNField>): Pair<NNCollection?, Exception?>

    /**
     * Deletes collection by name
     */
    fun deleteCollection(name: String): Exception?

}