package ua.com.avesoft.base.utils.contstants

import android.net.Uri

object Uris {

    private const val protocol = "content"
    private const val domain = "ua.com.avesoft.nnproject"
    private const val base = "$protocol://$domain"

    val GET_ALL_IMAGE = Uri.parse("$base/images/all")
    val GET_IMAGE = Uri.parse("$base/images/get")

    val ADD_IMAGE = Uri.parse("$base/images/add")
    val UPDATE_IMAGE = Uri.parse("$base/images/update")
    val REMOVE_IMAGE = Uri.parse("$base/images/remove")

    val SUCCESS = Uri.parse("$base/success")
    val ERROR = Uri.parse("$base/error")

    val B_APP_OPEN = Uri.parse("nn://ua.com.avesoft.nnproject")

}