package ua.com.avesoft.base.utils

import java.util.*

fun Long.dateStr(): String {
    val calendar = Calendar.getInstance()
    calendar.time = Date(this)
    return "${calendar.get(Calendar.DAY_OF_MONTH)} ${calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault())} ${calendar.get(Calendar.YEAR)}"
}

fun Long.timeStr(): String {
    val calendar = Calendar.getInstance()
    calendar.time = Date(this)
    return "${calendar.get(Calendar.HOUR_OF_DAY).toSpecialString()}:${calendar.get(Calendar.MINUTE).toSpecialString()}"
}

fun Int.toSpecialString(): String {
    if ("$this".length == 1) {
        return "0$this"
    }
    return this.toString()
}

fun<T, R>  Iterator<T>.map(transform: (T) -> R): List<R> {
    val list: MutableList<R> = mutableListOf()
    forEach { list += transform(it) }
    return list
}