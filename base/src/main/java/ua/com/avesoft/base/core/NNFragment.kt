package ua.com.avesoft.base.core

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v4.app.Fragment
import ua.com.avesoft.base.utils.T2Handler

abstract class NNFragment: Fragment(), NNFunctions {

    override fun context() = context ?: activity ?: throw NullPointerException("Context is null")


    /**
     * Receivers list
     */
    private val receivers: ArrayList<BroadcastReceiver> = arrayListOf()

    /**
     * Adds receivers, that will be removed in {@link NNFragment#onDestroy()}
     */
    protected fun initReceiver(action: String, callback: T2Handler<Context, Intent>) {
        val br = object: BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                callback(context, intent)
            }
        }
        val intFilter = IntentFilter(action)
        context?.apply {
            registerReceiver(br, intFilter)
            receivers += br
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        context?.apply { receivers.forEach { unregisterReceiver(it) } }
    }

}