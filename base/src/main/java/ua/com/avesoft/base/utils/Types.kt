package ua.com.avesoft.base.utils


typealias ConfigHandler<T> = T.() -> T

typealias Handler = RHandler<Unit>
typealias T1Handler<T> = T1RHandler<T, Unit>
typealias T2Handler<T1, T2> = T2RHandler<T1, T2, Unit>

typealias RHandler<T> = () -> T
typealias T1RHandler<T, R> = (T) -> R
typealias T2RHandler<T1, T2, R> = (T1, T2) -> R



typealias SuspendT1Handler<T> = SuspendT1RHandler<T, Unit>
typealias SuspendT2Handler<T1, T2> = SuspendT2RHandler<T1, T2, Unit>
typealias SuspendT3Handler<T1, T2, T3> = SuspendT3RHandler<T1, T2, T3, Unit>

typealias SuspendT1RHandler<T1, R> = suspend (T1) -> R
typealias SuspendT2RHandler<T1, T2, R> = suspend (T1, T2) -> R
typealias SuspendT3RHandler<T1, T2, T3, R> = suspend (T1, T2, T3) -> R