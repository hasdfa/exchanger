package com.vadim.hasdfa.exchanger.model

import ua.com.avesoft.base.database.FieldType
import ua.com.avesoft.base.database.NNEntity
import ua.com.avesoft.base.database.NNField
import java.math.BigDecimal

data class NetworkResult(
        var timestamp: Long = System.currentTimeMillis(),
        var base: String = CurrencyType.EUR.name,
        var date: String = "",
        var rates: Map<String, Double> = emptyMap(),
        var isSaved: Boolean = false
): NNEntity() {


    fun exchange(value: Double, from: CurrencyType, to: CurrencyType): Double {
        val eur = value / rates[from.name.toUpperCase()]!!
        return eur * rates[to.name.toUpperCase()]!!
    }



    private val splittor = "[{:}]"
    private val separator = "[{;}]"

    override val fields: List<NNField> = listOf(
            NNField("uid", uid, FieldType.TEXT),
            NNField("timestamp", timestamp, FieldType.LONG),
            NNField("base", base, FieldType.TEXT),
            NNField("date", date, FieldType.TEXT),
            NNField("rates", rates.map { "${it.key}$splittor${it.value}" }.joinToString(separator = separator), FieldType.TEXT)
    )

    override fun internalFillData(field: NNField) {
        when (field.name) {
            "timestamp" -> timestamp = field.get as Long
            "base" -> base = field.get.toString()
            "date" -> date = field.get.toString()
            "rates" -> {
                rates = field.get.toString()
                        .split(separator)
                        .map { it.split(splittor) }
                        .map { it.component1() to it.component2().toDouble() }
                        .toMap()
            }
        }
    }

    override fun validate(): Boolean {
        return true
    }
}