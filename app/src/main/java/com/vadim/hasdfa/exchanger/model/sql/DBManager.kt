package com.vadim.hasdfa.exchanger.model.sql

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import ua.com.avesoft.base.database.FieldType
import ua.com.avesoft.base.database.NNField
import com.vadim.hasdfa.exchanger.model.CurrencyModel
import com.vadim.hasdfa.exchanger.model.NetworkResult
import ua.com.avesoft.base.utils.contstants.Collections

/**
 * Created by Raksha Vadim on 03.08.17, 11:40.
 */

internal class DBManager(context: Context): SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        val err = createTable(db, Collections.CurrenciesHistory, NetworkResult().fields)
        err?.printStackTrace()
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        throw NotImplementedError()
    }


    /**
     * Create table with default {@link SQLiteOpenHelper#getWritableDatabase()}
     */
    fun createTable(tableName: String, params: List<NNField>): Exception? {
       return createTable(writableDatabase, tableName, params)
    }

    /**
     * Create table by name with fields
     *  with specific database: @param db
     *
     * @param tableName New table name
     * @param params Fields in new table
     */
    private fun createTable(db: SQLiteDatabase, tableName: String, params: List<NNField>): Exception? {
        try {
            val fields = params.filter { it.name != "uid" }.joinToString(separator = ",\n"){
                "  \"${it.name}\" ${fields(it.type)} ${ if (!it.nullable) "NOT NULL" else "" }"
            }
            db.execSQL("CREATE TABLE IF NOT EXISTS \"" + tableName + "\" (\n" +
                    "  \"uid\" text PRIMARY KEY NOT NULL,\n $fields);"
            )
        } catch (e: Exception) {
            return e
        }
        return null
    }

    /**
     * Drop table with specific name
     * @param tableName
     */
    fun dropTable(tableName: String): Exception? {
        try {
            writableDatabase.execSQL("DROP TABLE IF EXISTS $tableName", null)
        } catch (e: Exception) {
            return e
        }
        return null
    }

    /**
     * Get all tables from this database
     */
    fun allTables(): List<String> {
        val arrTblNames = ArrayList<String>()
        val c = readableDatabase.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null)
        c.use {
            if (c.moveToFirst()) {
                while (!c.isAfterLast) {
                    arrTblNames.add(c.getString(c.getColumnIndex("name")))
                    c.moveToNext()
                }
            }
        }
        return arrTblNames
    }

    companion object {
        /**
         * Default database name
         */
        private const val DATABASE_NAME = "NNImages.db"

        private const val DATABASE_VERSION = 1

        /**
         * Type wrappers for SQL
         *
         *  P.S. does not compile if not all types covered
         */
        private fun fields(type: FieldType) = when (type) {
                FieldType.TEXT -> "text"
                FieldType.INTEGER -> "integer"
                FieldType.FLOAT -> "float"
                FieldType.LONG -> "integer"
        }
    }
}
