package com.vadim.hasdfa.exchanger.model.sql

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import ua.com.avesoft.base.database.NNCollection
import ua.com.avesoft.base.database.NNEntity
import ua.com.avesoft.base.database.NNQueryParams
import ua.com.avesoft.base.database.NNSortParam
import ua.com.avesoft.base.utils.Database.generateWhereClause
import ua.com.avesoft.base.utils.Database.joinParams
import ua.com.avesoft.base.utils.Database.parseCursor
import ua.com.avesoft.base.utils.Database.translateSQL

internal class LocalCollection(
        private val db: SQLiteDatabase,
        private val tableName: String
): NNCollection
{
    /**
     * Main function, which querying raw sql to database, and receive cursor
     */
    private fun<T: NNEntity> query(clazz: Class<T>, sort: NNSortParam? = null, vararg params: NNQueryParams, additionalSql: String = ""): Cursor {
        val selected = (clazz.newInstance() as NNEntity).fields.joinToString(separator = ", ") { it.name }

        val raw = "SELECT $selected FROM $tableName ${generateWhereClause(params.toList())} $additionalSql ${generateSort(sort)}"
        Log.d("hasdfa", raw)
        return db.rawQuery(raw, null)
    }

    override fun <T : NNEntity> cursorAll(clazz: Class<T>, sort: NNSortParam?,  vararg params: NNQueryParams): Cursor {
        return query(clazz, sort, *params)
    }


    override fun<T: NNEntity> all(clazz: Class<T>, sort: NNSortParam?, vararg params: NNQueryParams): Pair<List<T>, Exception?> {
        return parseCursor(clazz, query(
                clazz = clazz,
                sort = sort,
                params = *params
        ))
    }


    override fun<T: NNEntity> one(clazz: Class<T>, sort: NNSortParam?, vararg params: NNQueryParams): Pair<T?, Exception?> {
        val (list, err) = parseCursor(clazz, query(
                clazz = clazz,
                sort = sort,
                additionalSql = "LIMIT 1",
                params = *params
        ))
        return Pair(list.firstOrNull(), err)
    }


    override fun<T: NNEntity> insert(t: T): Exception? {
        val names = t.fields.joinToString(separator = ", ") { it.name }
        val values = t.fields.joinToString(separator = ", ") { it.value?.translateSQL() ?: it.defaultValue.toString() }
        Log.d("hasdfa", t.fields.toString())
        Log.d("hasdfa", names)
        Log.d("hasdfa", values)

        try {
            db.execSQL("INSERT OR REPLACE INTO $tableName ($names) VALUES ($values)")
        } catch (e: Exception) {
            e.printStackTrace()
            return e
        }
        return null
    }


    override fun<T: NNEntity> save(t: T): Exception? {
        val set = t.fields.joinToString(separator = ", ") { "${it.name} = ${it.value?.translateSQL()}" }

        try {
            db.execSQL("UPDATE $tableName SET $set WHERE uid = '${t.uid}'")
        } catch (e: Exception) {
            e.printStackTrace()
            return e
        }
        return null
    }

    override fun removeId(uid: String): Pair<Int, Exception?> {
        return try {
            db.delete(tableName, "uid = '$uid'", null) to null
        } catch (e: Exception) {
            e.printStackTrace()
            0 to e
        }
    }


    override fun remove(vararg params: NNQueryParams): Pair<Int, Exception?> {
        if (params.count() == 0)
            return 0 to IllegalArgumentException("length of params should be greater than 0")
        return try {
            db.delete(tableName, joinParams(params.toList()), null) to null
        } catch (e: Exception) {
            e.printStackTrace()
            0 to e
        }
    }

    /**
     * Generates sort sql conditional from @{link NNSortParam}
     */
    private fun generateSort(sort: NNSortParam?): String {
        return if (sort == null) ""
        else "ORDER BY ${sort.name} ${sort.sortDirection}"
    }
}