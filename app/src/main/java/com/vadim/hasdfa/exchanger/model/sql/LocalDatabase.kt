package com.vadim.hasdfa.exchanger.model.sql

import android.content.Context
import ua.com.avesoft.base.database.NNCollection
import ua.com.avesoft.base.database.NNDatabase
import ua.com.avesoft.base.database.NNField

class LocalDatabase(private val context: Context): NNDatabase {

    private val db by lazy { DBManager(context) }

    override fun collections(): List<String> {
        return db.allTables()
    }

    override fun collection(name: String): NNCollection {
        return LocalCollection(db.writableDatabase, name)
    }

    override fun createCollection(name: String, fields: List<NNField>): Pair<NNCollection?, Exception?> {
        val err = db.createTable(name, fields)
        if (err != null) return null to err
        return collection(name) to null
    }

    override fun deleteCollection(name: String): Exception? {
        return db.dropTable(name)
    }
}