package com.vadim.hasdfa.exchanger.currencies.network

import android.net.Uri
import android.util.Log
import com.vadim.hasdfa.exchanger.model.NetworkResult
import ua.com.avesoft.base.utils.map
import ua.com.avesoft.base.utils.T2Handler
import java.net.HttpURLConnection
import java.net.URL
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import kotlin.concurrent.thread

object NetCurrency {

    private val apiUrl = Uri.parse("http://data.fixer.io/api/latest").buildUpon()
            .appendQueryParameter("access_key", "94f002efd5843f2292333449720e0324")
            .appendQueryParameter("symbols", "UAH,EGP,EUR,USD")
            .build().toString()

    fun request(callback: T2Handler<NetworkResult?, Exception?>) {
        Log.d("hasdfa", "Do Network request\n\n\n")
        thread {
            try {
                (URL(apiUrl).openConnection() as HttpURLConnection).apply {
                    requestMethod = "GET"

                    BufferedReader(InputStreamReader(inputStream)).useLines {
                        val json = JSONObject(it.joinToString(separator = ""))
                        val rates = json.getJSONObject("rates")

                        callback(NetworkResult(
                                timestamp = json.getLong("timestamp"),
                                base = json["base"].toString(),
                                date = json["date"].toString(),
                                rates = rates.keys()
                                        .map { it to rates[it].toString().toDouble() }
                                        .toMap()
                        ), null)
                    }
                }
            } catch (e: Exception) {
                callback(null, e)
            }
        }
    }

}