package com.vadim.hasdfa.exchanger.controllers

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.vadim.hasdfa.exchanger.R
import com.vadim.hasdfa.exchanger.controllers.adapters.MainAdapter
import com.vadim.hasdfa.exchanger.currencies.database.DBCurrency
import com.vadim.hasdfa.exchanger.currencies.network.NetCurrency
import com.vadim.hasdfa.exchanger.model.NetworkResult
import kotlinx.android.synthetic.main.activity_main.*
import ua.com.avesoft.base.core.NNActivity
import ua.com.avesoft.base.utils.T2Handler
import java.util.*

class MainActivity: NNActivity() {

    private var isErrorWas = false
    private var model: NetworkResult? = null

    private val mAdapter by lazy {
        MainAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mRecyclerView.adapter = mAdapter

        swipeRefresh.setOnRefreshListener {
            model = null.apply {
                request()
            }
        }
    }

    private val onResponse: T2Handler<NetworkResult?, Exception?> =  { result: NetworkResult?, err: Exception? ->
        runOnUiThread {
            if (!isDestroyed) {
                swipeRefresh.isRefreshing = false

                if (err != null) {
                    if (isErrorWas)
                        showError(err.localizedMessage) {
                            finishAffinity()
                        }
                    else {
                        networkRequest()
                        isErrorWas = true
                    }
                } else {
                    model = result?.also {
                        Log.d("hasdfa", "Latest: ${it.date}, now: ${today()}")
                        if (it.isSaved && today() != it.date)
                            networkRequest()

                        mAdapter.notifyUpdated(it)
                        isErrorWas = false
                        if (it.isSaved.not())
                            DBCurrency.save(this, it) { err ->
                                Log.d("hasdfa", err?.localizedMessage ?: "YEAH")
                            }
                    }
                }
            }
        }
    }

    private fun today(): String {
        val calendar = Calendar.getInstance()
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        var month = (calendar.get(Calendar.MONTH) + 1).toString()
        val year = calendar.get(Calendar.YEAR)

        if (month.length == 1)
            month = "0$month"

        return "$year-$month-$day"
    }

    private fun request() {
        if (model != null)
            onResponse(model, null)
        else {
            isErrorWas = false
            databaseRequest()
        }
    }


    override fun onStart() {
        super.onStart()
        mAdapter.notifyDataSetChanged()
        request()
    }


    private fun networkRequest() {
        NetCurrency.request(onResponse)
    }

    private fun databaseRequest() {
        DBCurrency.request(this, onResponse)
    }
}