package com.vadim.hasdfa.exchanger.controllers.adapters

import android.content.Context
import android.content.res.ColorStateList
import android.provider.Settings
import android.support.v4.content.ContextCompat
import android.support.v7.widget.AppCompatEditText
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.vadim.hasdfa.exchanger.R
import com.vadim.hasdfa.exchanger.model.CurrencyModel
import com.vadim.hasdfa.exchanger.model.CurrencyType
import com.vadim.hasdfa.exchanger.model.NetworkResult
import ua.com.avesoft.base.utils.T1Handler
import ua.com.avesoft.base.utils.toSpecialString
import java.math.BigDecimal
import java.math.MathContext

class MainAdapter(
        private var result: NetworkResult = NetworkResult(),
        private var currencies: List<CurrencyModel> = emptyList()
): RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    private val onChangeCurrencies: HashMap<CurrencyType, T1Handler<Double>> = hashMapOf()
    var currentValue = 0.0
    var active: CurrencyType = CurrencyType.Undefined

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (viewType == -1)
            return ViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_date, parent, false))

        return ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_currency, parent, false))
    }

    override fun getItemCount(): Int {
        return currencies.count() + 1
    }

    override fun getItemViewType(position: Int): Int {
        if (position == currencies.count())
            return -1
        return 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position == currencies.count()) {
            (holder.itemView as AppCompatTextView).apply {
                text = context.getString(R.string.updated_on, result.date.split("-").reversed().joinToString("."))
            }
            return
        }

        val data = currencies[position]
        with(holder) {
            flagImage.setImageDrawable(ContextCompat.getDrawable(context, data.flagFile))
            currencyLogoImage.setImageDrawable(ContextCompat.getDrawable(context, data.iconFile))
            setupIfNeedIt(holder)

            currencyLabel.text = data.type.toString()
            currencyValue.setOnFocusChangeListener { _, hasFocus ->
                currencyLogoImage.setColorFilter(ContextCompat.getColor(context,
                       if (hasFocus) R.color.colorAccent
                       else R.color.colorPrimary
                ))
                if (hasFocus) {
                    active = data.type
                    currencyValue.setText(
                            if (currentValue == 0.0) ""
                            else currentValue.toString()
                    )
                    Log.d("hasdfa", "Set active to ${active.name}")
                }
            }
            currencyValue.addTextChangedListener(object: TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    if (currencyValue.isFocused && active == data.type)
                        data.notifyCurrencyChanges(
                                (s?.toString()?.toDoubleOrNull() ?: 0.0)
                        )
                }
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            })
            onChangeCurrencies[data.type] = {
                var value = (Math.round(it * 100.00) / 100.00).toString()
                if (!value.contains("."))
                    value += ".00"
                else if ((value.split(".")[1].count() == 1))
                    value += "0"
                currencyValue.setText(value)
            }
        }
    }

    fun notifyUpdated(result: NetworkResult) {
        this.result = result
        currencies = result.rates.map {
            CurrencyModel.new(CurrencyType.valueOf(it.key.toUpperCase()))
        }
        notifyDataSetChanged()
    }

    private fun CurrencyModel.notifyCurrencyChanges(value: Double) {
        currentValue = value
        onChangeCurrencies.forEach {
            if (it.key != type)
                it.value(result.exchange(value, type, it.key))
        }
    }

    private var currencyLabelSize = 0f
    private var currencyValueSize = 0f

    private fun setupIfNeedIt(holder: ViewHolder) {
        try {
            val scale = Settings.System
                    .getFloat(holder.context.contentResolver, Settings.System.FONT_SCALE) * 2
            if (scale == 2.0f) return
            with(holder) {
                if (currencyLabelSize == 0f)
                    currencyLabelSize = currencyLabel.textSize / scale

                if (currencyValueSize == 0f)
                    currencyValueSize = currencyValue.textSize / scale

                currencyLabel.textSize = currencyLabelSize
                currencyValue.textSize = currencyValueSize
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val flagImage = view.findViewById<AppCompatImageView>(R.id.flagImage)
        val currencyLabel = view.findViewById<TextView>(R.id.currencyLabel)
        val currencyLogoImage = view.findViewById<AppCompatImageView>(R.id.currencyLogoImage)

        val currencyValue = view.findViewById<AppCompatEditText>(R.id.currencyValue)

        val context: Context
            get() = itemView.context
    }
}