package com.vadim.hasdfa.exchanger.currencies.database

import android.content.Context
import android.util.Log
import com.vadim.hasdfa.exchanger.model.NetworkResult
import com.vadim.hasdfa.exchanger.model.sql.LocalDatabase
import ua.com.avesoft.base.database.NNDatabase
import ua.com.avesoft.base.database.NNSortParam
import ua.com.avesoft.base.database.SortDirection
import ua.com.avesoft.base.utils.T1Handler
import ua.com.avesoft.base.utils.T2Handler
import ua.com.avesoft.base.utils.contstants.Collections
import kotlin.concurrent.thread

object DBCurrency {

    private fun db(context: Context): NNDatabase
        = LocalDatabase(context)

    fun request(context: Context, callback: T2Handler<NetworkResult?, Exception?>) {
        Log.d("hasdfa", "Do SQL Database request\n\n\n")
        thread {
            try {
                db(context).collection(Collections.CurrenciesHistory).apply {
                    val (result, err) = all(
                            clazz = NetworkResult::class.java,
                            sort = NNSortParam("timestamp", SortDirection.DESC)
                    )
                    val model = result.firstOrNull()?.also { it.isSaved = true }
                    callback(model, err ?: (if (model == null) NullPointerException("No such record") else null))
                }
            } catch (e: Exception) {
                callback(null, e)
            }
        }
    }

    fun save(context: Context, item: NetworkResult, callback: T1Handler<Exception?>) {
       thread {
           try {
               db(context).collection(Collections.CurrenciesHistory).apply {
                   callback(insert(item))
               }
           } catch (e: Exception) {
               callback(e)
           }
       }
    }

}