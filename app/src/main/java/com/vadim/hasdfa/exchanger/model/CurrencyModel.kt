package com.vadim.hasdfa.exchanger.model

import android.support.annotation.DrawableRes
import com.vadim.hasdfa.exchanger.R

data class CurrencyModel(
        var type: CurrencyType = CurrencyType.Undefined,

        @DrawableRes
        var flagFile: Int = R.drawable.ic_launcher_background,

        @DrawableRes
        var iconFile: Int = R.drawable.ic_launcher_foreground
) {

    companion object {
        fun new(type: CurrencyType) = CurrencyModel(
                type = type,
                flagFile = flagOf(type),
                iconFile = iconOf(type)
        )

        private fun flagOf(type: CurrencyType): Int
            = when (type) {
                CurrencyType.UAH -> R.drawable.flag_ua
                CurrencyType.EGP -> R.drawable.flag_eg
                CurrencyType.EUR -> R.drawable.flag_eu
                CurrencyType.USD -> R.drawable.flag_us
                CurrencyType.Undefined -> R.drawable.ic_launcher_background
            }

        private fun iconOf(type: CurrencyType): Int
            = when (type) {
                CurrencyType.UAH -> R.drawable.ic_currency_uah
                CurrencyType.EGP -> R.drawable.ic_currency_egp
                CurrencyType.EUR -> R.drawable.ic_currency_eur
                CurrencyType.USD -> R.drawable.ic_currency_usd
                CurrencyType.Undefined -> R.drawable.ic_launcher_foreground
            }
    }
}

enum class CurrencyType {
    UAH, EGP, EUR, USD, Undefined
}